<html>

<head>
    <title>{{ config('app.name') }} | API's Swagger</title>
    <link href="{{asset('swagger/style.css')}}" rel="stylesheet">
    <link href="https://static1.smartbear.co/swagger/media/assets/swagger_fav.png" type="image/png" rel="icon"/>
</head>

<body>
    <div id="swagger-ui"></div>
    <script src="{{asset('swagger/jquery-2.1.4.min.js')}}"></script>
    <script src="{{asset('swagger/swagger-bundle.js')}}"></script>
    <script type="application/javascript">
        const ui = SwaggerUIBundle({
            url: "{{ asset('swagger/swagger.yaml') }}",
            dom_id: '#swagger-ui',
        });
    </script>
</body>

</html>