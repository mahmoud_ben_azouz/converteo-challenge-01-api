-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 19 oct. 2021 à 21:04
-- Version du serveur :  10.4.17-MariaDB
-- Version de PHP : 8.0.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `challenge`
--
CREATE DATABASE challenge;
USE challenge;
-- --------------------------------------------------------

--
-- Structure de la table `companys`
--

CREATE TABLE `companys` (
  `company_id` int(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `company_location` int(255) NOT NULL,
  `industry_company` int(255) NOT NULL,
  `company_type` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `companys`
--

INSERT INTO `companys` (`company_id`, `company_name`, `company_location`, `industry_company`, `company_type`) VALUES
(1, 'Greystone Technology', 1, 3, 'Public'),
(2, 'ChiliAppleBees', 2, 4, 'Public'),
(3, 'Oracle', 3, 6, 'Private'),
(4, 'University of tennessee chattanooga', 4, 2, 'Private');

-- --------------------------------------------------------

--
-- Structure de la table `currencys`
--

CREATE TABLE `currencys` (
  `currency_id` int(255) NOT NULL,
  `currency_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `currencys`
--

INSERT INTO `currencys` (`currency_id`, `currency_name`) VALUES
(1, 'USD'),
(2, 'GBP'),
(3, 'CAD'),
(4, 'EUR');

-- --------------------------------------------------------

--
-- Structure de la table `employes`
--

CREATE TABLE `employes` (
  `id_employe` int(255) NOT NULL,
  `age` varchar(255) NOT NULL,
  `industry_id` int(255) NOT NULL,
  `job_title` varchar(255) NOT NULL,
  `annual_salary` varchar(255) NOT NULL,
  `currency` varchar(255) NOT NULL,
  `location_id` int(255) NOT NULL,
  `work_experience` varchar(255) NOT NULL,
  `job_description` int(255) NOT NULL,
  `other_currency` varchar(255) NOT NULL,
  `employment_id` int(255) NOT NULL,
  `company_id` int(255) NOT NULL,
  `gender` varchar(255) NOT NULL,
  `hours_per_week` varchar(255) NOT NULL,
  `actual_hours_per_week` varchar(255) NOT NULL,
  `education` varchar(255) NOT NULL,
  `health_insurance` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `employes`
--

INSERT INTO `employes` (`id_employe`, `age`, `industry_id`, `job_title`, `annual_salary`, `currency`, `location_id`, `work_experience`, `job_description`, `other_currency`, `employment_id`, `company_id`, `gender`, `hours_per_week`, `actual_hours_per_week`, `education`, `health_insurance`) VALUES
(1, '35-44', 1, 'ingénieur', '75000', '1', 3, '11 - 20 years', 0, '', 1, 2, 'Male', '40+', '40-50', 'Graduate Degree', 'Yes'),
(2, '25-34', 4, 'ingénieur', '36000', '4', 5, '8 - 10 years', 0, '', 4, 3, 'Male', '40+', '50-70', 'Graduate Degree', 'No'),
(3, '45-54', 6, 'Senior Vice President', '40000', '3', 5, '11 - 20 years', 0, '', 3, 1, 'Female', '40+', '40-50', 'Undergraduate Degree', 'Yes');

-- --------------------------------------------------------

--
-- Structure de la table `employments`
--

CREATE TABLE `employments` (
  `employment_id` int(255) NOT NULL,
  `employment_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `employments`
--

INSERT INTO `employments` (`employment_id`, `employment_name`) VALUES
(1, 'Full-time'),
(2, 'Summer intern'),
(3, 'Contractor'),
(4, 'Part-time'),
(5, 'Self-Employed');

-- --------------------------------------------------------

--
-- Structure de la table `industries`
--

CREATE TABLE `industries` (
  `industry_id` int(255) NOT NULL,
  `industry_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `industries`
--

INSERT INTO `industries` (`industry_id`, `industry_name`) VALUES
(1, 'Government'),
(2, 'Environmental nonprofit'),
(3, 'Market Research'),
(4, 'Biotechnology'),
(5, 'Healthcare'),
(6, 'Information Management');

-- --------------------------------------------------------

--
-- Structure de la table `locations`
--

CREATE TABLE `locations` (
  `location_id` int(255) NOT NULL,
  `location_name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `locations`
--

INSERT INTO `locations` (`location_id`, `location_name`) VALUES
(1, 'Nashville, TN'),
(2, 'Madison, Wi'),
(3, 'Las Vegas, NV'),
(4, 'Cardiff, UK'),
(5, 'Southeast Michigan, USA'),
(6, 'Seattle, WA'),
(7, 'Dallas, Texas, United States'),
(8, 'Philadelphia');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `companys`
--
ALTER TABLE `companys`
  ADD PRIMARY KEY (`company_id`);

--
-- Index pour la table `currencys`
--
ALTER TABLE `currencys`
  ADD PRIMARY KEY (`currency_id`);

--
-- Index pour la table `employes`
--
ALTER TABLE `employes`
  ADD PRIMARY KEY (`id_employe`);

--
-- Index pour la table `employments`
--
ALTER TABLE `employments`
  ADD PRIMARY KEY (`employment_id`);

--
-- Index pour la table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`industry_id`);

--
-- Index pour la table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`location_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `companys`
--
ALTER TABLE `companys`
  MODIFY `company_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `currencys`
--
ALTER TABLE `currencys`
  MODIFY `currency_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `employes`
--
ALTER TABLE `employes`
  MODIFY `id_employe` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `employments`
--
ALTER TABLE `employments`
  MODIFY `employment_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `industries`
--
ALTER TABLE `industries`
  MODIFY `industry_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `locations`
--
ALTER TABLE `locations`
  MODIFY `location_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
