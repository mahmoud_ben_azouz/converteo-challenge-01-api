<p align="center">
<a href="https://converteo.com" target="_blank"><img src="https://converteo.com/wp-content/themes/converteo/assets/images/converteo_logo_white.png" width="400"></a></p>


## Challenge 1


To access this project you need to build then to run it.<br>
If you have an environment ready to execute laravel project you can start by:<br>
1 - Build project: <br>
make build<br>
2 - Run the project:<br>
make run<br><br>

If your environment is not configured to execute laravel projects, you can simply install docker and then just put these two commands to build and run.<br>
1 - Build: <br>
make build-with-docker<br>
2 - Run:<br>
make run-with-docker<br><br>

On executing with docker, you can access the project by this URL:<br>
http://localhost/docs<br>
For the demo, the project was depolyed on an EC2 instance, you can access by this URL:<br>
http://ec2-13-37-231-10.eu-west-3.compute.amazonaws.com/docs<br>
or watch the demo.mp4 file <br><br>

have fun 😉<br><br><br>

## Challenge 2

1) You find the database schema "challenge.sql" in the directory challenge2
2) you find the schema diagram "diagram.png" in the directory challenge2
<p><img src="./challenge2/diagramme.png"></p>
3) See the database schema <br>
4)
 <p>SELECT AVG(`annual_salary`) as annual_salary FROM `employes` WHERE `job_title` ="ingénieur";</p>
 <p><img src="./challenge2/r_1.png"></p>
 <p>SELECT loc.`location_name`, MIN(`annual_salary`) as min_salary,MAX(`annual_salary`) as max_salary,AVG(`annual_salary`) as avg_salary FROM `employes` as emp,`locations` as loc WHERE emp.`location_id`=loc.`location_id` GROUP BY loc.`location_name` ;</p>
 <p><img src="./challenge2/r_2.png"></p>
 
 <p> SELECT `gender`,AVG(`annual_salary`) AS annual_salary FROM `employes` GROUP BY `gender`;</p>
 <p><img src="./challenge2/r_3.png"></p>
