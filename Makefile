default: build

include .env
export

app=challenge-api-app
port=80


build:
	composer install


build-with-docker:
	docker build -t ${app}-image .


run:
	php artisan serve

run-with-docker:
	docker run  -v pwd:/app -p ${port}:8000 -d --name ${app} ${app}-image


clean-with-docker:
	docker stop ${app}
	docker rm ${app}
	docker rmi ${app}-image