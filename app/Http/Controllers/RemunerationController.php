<?php

namespace App\Http\Controllers;

use App\Http\Requests\RemunerationRequest;
use Illuminate\Support\Facades\Session;


class RemunerationController extends Controller
{

    public function getData(RemunerationRequest $request)
    {
        // Read File
        if (Session::has(request('survery_file'))) {
            $collection = Session::get(request('survery_file'));
        } else {
            $collection = getFile();
        }

        if (request('search')) {
            $collection = $collection->filter(function ($item) {
                return strpos(implode($item), request('search')) !== false;
            });
        }
        if (request('fields')) {
            $collection = $collection->map(function ($item) {
                return collect($item)
                    ->only(explode(',', request('fields')))
                    ->all();
            });
        }
        if (request('sort')) {
            $collection = $collection->sortBy([
                [request('sort'), 'asc'],
            ]);
        }
        if (request('key_value')) {
            if (str_contains(request('key_value'), ',')) {
                $keys_values = explode(',', request('key_value'));
                foreach ($keys_values as $item) {
                    $key_value = explode(':', $item);
                    $collection = $collection->where($key_value[0], $key_value[1]);
                }
            } else {
                $key_value = explode(':', request('key_value'));
                $collection = $collection->where($key_value[0], $key_value[1]);
            }
        }

        return $collection->values()->all();
    }
}
