<?php

use Illuminate\Support\Facades\Session;

function getFile()
{
    $jsonString = file_get_contents(base_path('resources/dataset/' . request('survery_file') . '.json.txt'));
    $data = json_decode($jsonString, true);

    Session::put(request('survery_file'), collect($data));
    return Session::get(request('survery_file'));
}
