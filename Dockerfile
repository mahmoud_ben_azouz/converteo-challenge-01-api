FROM php:8.0-fpm-alpine
WORKDIR /app
COPY . ./
RUN docker-php-ext-install pdo
RUN curl -sS https://getcomposer.org/installer | \
            php -- --install-dir=/usr/bin/ --filename=composer
RUN composer install
EXPOSE 8000
CMD ["php", "artisan", "serve","--host=0.0.0.0"]


